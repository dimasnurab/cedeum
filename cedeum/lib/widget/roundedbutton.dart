import 'package:flutter/material.dart';

class ButtonRounded extends StatelessWidget {
  const ButtonRounded({
    this.buttonColor,
    this.marginBottom,
    this.marginLeft,
    this.marginRight,
    this.marginTop,
    this.onTap,
    this.textButton,
    this.colorText,
    this.width,
  });
  final double marginTop;
  final double marginRight;
  final double marginLeft;
  final double marginBottom;
  final double width;
  final Color buttonColor;
  final Color colorText;
  final Function onTap;
  final String textButton;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        // height: height ?? 36,
        width: width ?? double.infinity,
        padding: EdgeInsets.symmetric(vertical: 13),
        margin: EdgeInsets.only(
            left: marginLeft ?? 0,
            right: marginRight ?? 0,
            top: marginTop ?? 0,
            bottom: marginBottom ?? 0),
        decoration: BoxDecoration(
          color: buttonColor ?? Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Center(
          child: Text(
            textButton ?? '',
            style: Theme.of(context).textTheme.headline2.copyWith(
                  fontSize: 14,
                  color: colorText ?? Theme.of(context).primaryColor,
                ),
          ),
        ),
      ),
    );
  }
}
