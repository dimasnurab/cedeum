import 'package:flutter/material.dart';

DrawerHeader drawerheadercustom(BuildContext context) {
  return DrawerHeader(
    duration: const Duration(milliseconds: 1000),
    decoration: BoxDecoration(
      color: Theme.of(context).accentColor,
    ),
    child: Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('CEDEUM',
                style: Theme.of(context).textTheme.bodyText2.copyWith(
                      fontSize: 24,
                      letterSpacing: 1.75,
                      color: Theme.of(context).primaryColor,
                    )),
            SizedBox(height: 4),
            Text('aplikasi cuaca yang menemani hari harimu',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline4.copyWith(
                      fontSize: 12,
                      letterSpacing: 1,
                      color: Theme.of(context).primaryColor,
                    )),
          ],
        ),
      ),
    ),
  );
}

Container builditemdrawer(
  BuildContext context, {
  String title,
  Widget child,
  TextStyle titleStyle,
}) {
  return Container(
    margin: EdgeInsets.fromLTRB(14, 0, 14, 0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 175,
          child: Text(title,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: titleStyle ??
                  Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12)),
        ),
        child ?? Container(),
      ],
    ),
  );
}
