import 'package:cedeum/theme/constant.dart';
import 'package:cedeum/widget/roundedbutton.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

Widget allerDialogOnebutton({
  VoidCallback onTap,
  String title,
  @required BuildContext context,
}) =>
    AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      content: Text(
        title,
        style: Theme.of(context).textTheme.headline4.copyWith(fontSize: 15),
      ),
      actions: <Widget>[
        SizedBox(height: 16),
        GestureDetector(
          onTap: onTap,
          child: Container(
            margin: EdgeInsets.only(left: 4, right: 2, bottom: 4),
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Text(
              'OK',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 13,
                    color: Theme.of(context).primaryColor,
                  ),
            ),
          ),
        ),
      ],
    );

Widget allertFailedStatus({
  VoidCallback onTap,
  String titleButton,
  dynamic statusCode,
  dynamic msg,
  @required double height,
  @required double width,
  @required BuildContext context,
}) =>
    Center(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 50),
        padding: EdgeInsets.all(14),
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Ooops, ' + statusCode.toString(),
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 26,
                      color: kDarker.withOpacity(0.8),
                    ),
              ),
            ),
            SizedBox(height: 30),
            Expanded(
              flex: 2,
              child: Text(
                msg.toString(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: 20,
                      color: kDarker.withOpacity(0.8),
                    ),
              ),
            ),
            Expanded(
              child: ButtonRounded(
                buttonColor: kDarker,
                onTap: onTap,
                textButton: titleButton,
                colorText: kWhite,
              ),
            ),
          ],
        ),
      ),
    );
