import 'package:flutter/material.dart';

class CustomTextFormfield extends StatelessWidget {
  const CustomTextFormfield({
    Key key,
    this.autoCorrect,
    this.autoValidate,
    this.backgroundColorField,
    this.colorBorder,
    this.controller,
    this.enabled,
    this.hintStyle,
    this.hintText,
    this.keyboardType,
    this.labelText,
    this.marginBot,
    this.marginLeft,
    this.marginRight,
    this.marginTop,
    this.maxLength,
    this.obsecureText,
    this.onChanged,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.onSaved,
    this.paddingLeft,
    this.prefixIcon,
    this.style,
    this.suffixIcon,
    this.width,
    this.widthBorder,
    this.focusNode,
    this.textInputAction,
  }) : super(key: key);

  final FocusNode focusNode;
  final Color backgroundColorField;
  final int maxLength;
  final double width;
  final double marginTop;
  final double marginBot;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final Function(String) onSaved;
  final Function(String) onChanged;
  final Function(String) onFieldSubmitted;
  final Function onEditingComplete;
  final bool enabled;
  final bool obsecureText;
  final String labelText;
  final String hintText;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final double marginLeft;
  final double marginRight;
  final double widthBorder;
  final double paddingLeft;
  final Color colorBorder;
  final TextStyle style;
  final TextStyle hintStyle;
  final bool autoCorrect;
  final bool autoValidate;
  final TextInputAction textInputAction;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: paddingLeft ?? 0),
      margin: EdgeInsets.only(
          top: marginTop ?? 0,
          left: marginLeft ?? 0,
          right: marginRight ?? 0,
          bottom: marginBot ?? 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: backgroundColorField ?? Colors.transparent,
        border: Border.all(
          color: colorBorder,
          width: widthBorder ?? 2,
        ),
      ),
      child: TextFormField(
        textInputAction: textInputAction,
        focusNode: focusNode,
        maxLength: maxLength,
        onFieldSubmitted: onFieldSubmitted,
        autocorrect: autoCorrect ?? false,
        onEditingComplete: onEditingComplete,
        enabled: enabled ?? true,
        controller: controller,
        keyboardType: keyboardType ?? TextInputType.text,
        onSaved: onSaved,
        onChanged: onChanged,
        obscureText: obsecureText ?? false,
        style: style,
        decoration: InputDecoration(
          labelText: labelText,
          hintText: hintText,
          border: InputBorder.none,
          hintStyle: hintStyle,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          counterText: '',
        ),
      ),
    );
  }
}
