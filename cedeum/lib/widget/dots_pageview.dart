import 'package:cedeum/shared/size_config.dart';
import 'package:flutter/material.dart';

class DotsPageView extends StatefulWidget {
  final int dotsItems;
  final int currentIndex;
  const DotsPageView({
    this.currentIndex = 0,
    this.dotsItems,
  });
  @override
  _DotsPageViewState createState() => _DotsPageViewState();
}

class _DotsPageViewState extends State<DotsPageView> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(
          top: getProportionateScreenHeight(20),
          bottom: getProportionateScreenHeight(20)),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (var i = 0; i < widget.dotsItems; i++)
            _DotsItems(
              selected: i == widget.currentIndex,
            ),
        ],
      ),
    );
  }
}

class _DotsItems extends StatelessWidget {
  const _DotsItems({
    this.selected,
    this.scale = 1.0,
  });

  final double scale;

  final bool selected;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 750),
      height: 10,
      width: selected ? 10 : 15,
      margin: EdgeInsets.only(right: 4),
      decoration: BoxDecoration(
        color: selected
            ? Theme.of(context).accentColor
            : Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(selected ? 20 : 4),
      ),
    );
  }
}
