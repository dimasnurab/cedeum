import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StackWithProgress extends StatelessWidget {
  const StackWithProgress({
    Key key,
    @required List<Widget> children,
    bool isLoading,
  })  : assert(children != null),
        _children = children,
        _isLoading = isLoading ?? false,
        super(key: key);

  final List<Widget> _children;
  final bool _isLoading;

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      _children.add(_progressBar());
    }
    return Stack(
      children: _children,
    );
  }

  Widget _progressBar() {
    return Stack(
      children: [
        Opacity(
          opacity: 0.8,
          child: ModalBarrier(
            dismissible: false,
            color: Colors.white,
          ),
        ),
        Center(
          child: Platform.isAndroid
              ? CircularProgressIndicator()
              : CupertinoActivityIndicator(),
        ),
      ],
    );
  }
}
