import 'package:flutter/material.dart';

import 'package:sliding_up_panel/sliding_up_panel.dart';

class SlidingUpCustom extends StatelessWidget {
  const SlidingUpCustom({
    Key key,
    this.onPanelClosed,
    this.onPanelOpened,
    this.children,
    this.maxHeight,
    this.minHeight,
    @required this.isOpenedPanel,
  }) : super(key: key);

  final Function onPanelOpened;
  final Function onPanelClosed;
  final bool isOpenedPanel;
  final double maxHeight;
  final double minHeight;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
      onPanelOpened: onPanelOpened,
      onPanelClosed: onPanelClosed,
      color: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
      maxHeight: maxHeight,
      minHeight: minHeight,
      panel: Container(
        margin: EdgeInsets.only(
          top: 10,
          right: 24,
          left: 24,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: children,
          ),
        ),
      ),
    );
  }
}
