import 'dart:async';

import 'package:cedeum/services/location.dart';
import 'package:cedeum/services/networkservices.dart';
import 'package:cedeum/services/responseserver.dart';
import 'package:cedeum/shared/baseconfig.dart';
import 'package:cedeum/shared/canonical_path.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ThemeStatus { light, dark }

class WeatherRepositories extends NetworkServices {
  final _controller = StreamController<ThemeStatus>();

  Location _location = Location();

  Stream<ThemeStatus> get status async* {
    bool result = await getTheme();
    if (result) {
      yield ThemeStatus.dark;
    } else {
      yield ThemeStatus.light;
    }

    yield* _controller.stream;
  }

  Future<bool> getTheme() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool('theme') ?? false;
  }

  Future<ResponseServer> fetchCurrentPosition() async {
    await _location.getCurrentLocation();
    var queryString = {
      'lat': _location.latitude,
      'lon': _location.longitude,
      'appid': BaseConfig.apiKey,
      'lang': 'id',
    };

    final response = await request(
      'GET',
      CanonicalPath.oneCall,
      queryString: queryString,
    );

    return ResponseServer(
      data: response.data,
      header: response.header,
      statusCode: response.statusCode,
    );
  }

  Future<ResponseServer> fetchByCity(String city) async {
    var queryString = {
      'q': city,
      'appid': BaseConfig.apiKey,
      'lang': 'id',
    };

    final response = await request(
      'GET',
      CanonicalPath.weather,
      queryString: queryString,
    );

    return ResponseServer(
      data: response.data,
      header: response.header,
      statusCode: response.statusCode,
    );
  }

  void dispose() => _controller.close();
}
