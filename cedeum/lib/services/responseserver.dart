import 'package:equatable/equatable.dart';

class ResponseServer extends Equatable {
  final dynamic data;
  final dynamic header;
  final dynamic statusCode;

  const ResponseServer({this.data, this.header, this.statusCode});

  @override
  List<Object> get props => [data, header, statusCode];

  @override
  String toString() {
    return 'headers : $header\ncontent : $data\nstatusCode : $statusCode';
  }
}
