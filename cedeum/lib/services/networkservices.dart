import 'package:cedeum/services/networkexception.dart';
import 'package:cedeum/services/responseserver.dart';
import 'package:cedeum/shared/baseconfig.dart';
import 'package:dio/dio.dart';

import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class NetworkServices {
  Dio _dio;

  NetworkServices() {
    BaseOptions options = new BaseOptions(
      baseUrl: BaseConfig.baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    this._dio = Dio(options);
    this._dio.interceptors.add(PrettyDioLogger(
          requestBody: true,
          requestHeader: true,
          responseHeader: true,
          maxWidth: 180,
        ));
  }

  Future<dynamic> request(
    String method,
    String path, {
    String contentType,
    Map<String, dynamic> queryString,
    dynamic content,
  }) async {
    Response responseJson;

    try {
      if (method == 'POST' || method == 'PUT') {
        responseJson =
            await _dio.post(path, data: content, queryParameters: queryString);
      } else {
        responseJson = await _dio.get(path, queryParameters: queryString);
      }
      if (responseJson.statusCode >= 200 && responseJson.statusCode <= 300) {
        if (responseJson.data != null) {
          return ResponseServer(
            data: responseJson.data,
            header: responseJson.headers,
            statusCode: responseJson.statusCode,
          );
        }
      }
    } on DioError catch (e) {
      _returnResponseError(e);
    }
  }

  dynamic _returnResponseError(DioError e) {
    String messageDefault =
        'Koneksi Terputus.\nSilahkan Coba beberapa saat lagi';
    switch (e.type) {
      case DioErrorType.CONNECT_TIMEOUT:
        throw NetworkException(
            httpStatus: 500,
            responseCode: '500',
            responseMessage: messageDefault);
        break;
      case DioErrorType.SEND_TIMEOUT:
        throw NetworkException(
            httpStatus: 500,
            responseCode: '500',
            responseMessage: messageDefault);
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        throw NetworkException(
            httpStatus: 500,
            responseCode: '500',
            responseMessage: messageDefault);
        break;
      case DioErrorType.RESPONSE:
        if (e.response.statusCode == 502) {
          throw NetworkException(
              httpStatus: e.response.statusCode,
              responseCode: '502',
              responseMessage: 'FORBIDDEN ACCESS');
        }
        if (e.response.statusCode == 500) {
          throw NetworkException(
              httpStatus: e.response.statusCode,
              responseCode: '500',
              responseMessage: messageDefault);
        }
        if (e.response.statusCode == 404) {
          throw NetworkException(
              httpStatus: e.response.statusCode,
              responseCode: e.response.data['cod'],
              responseMessage: 'Lokasi Tidak Ditemukan');
        }
        throw NetworkException(
            httpStatus: e.response.statusCode,
            responseCode: e.response.data['cod'],
            responseMessage: 'Bad Request');

        break;
      case DioErrorType.CANCEL:
        throw NetworkException(
            httpStatus: 408,
            responseCode: '500',
            responseMessage: 'Silahkan Periksa kembali jaringan internet anda');
        break;
      case DioErrorType.DEFAULT:
        if (e.message.contains('SocketException')) {
          throw NetworkException(
              httpStatus: 500,
              responseCode: '500',
              responseMessage:
                  'Silahkan Periksa kembali jaringan internet anda');
        }
        break;
    }
  }
}
