part of 'theme_bloc.dart';

class ThemeState extends Equatable {
  final ThemeStatus status;

  ThemeState({this.status = ThemeStatus.dark});

  ThemeState update({ThemeStatus status}) => copyWith(
        status: status,
      );

  ThemeState copyWith({ThemeStatus status}) =>
      ThemeState(status: status ?? this.status);

  @override
  List<Object> get props => [status];
}
