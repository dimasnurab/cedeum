import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cedeum/repositories/weather_repositories.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc({@required WeatherRepositories repos})
      : assert(repos != null),
        _repos = repos,
        super(ThemeState()) {
    _subscription = _repos.status.listen((event) => add(ChangeTheme(event)));
  }

  final WeatherRepositories _repos;
  StreamSubscription<ThemeStatus> _subscription;

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if (event is ChangeTheme) {
      switch (event.status) {
        case ThemeStatus.dark:
          yield state.update(status: ThemeStatus.dark);
          break;
        case ThemeStatus.light:
          yield state.update(status: ThemeStatus.light);
          break;
        default:
          yield state.update(status: ThemeStatus.light);
      }
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _repos.dispose();
    return super.close();
  }
}
