import 'package:cedeum/theme/constant.dart';
import 'package:flutter/material.dart';

ThemeData themeData(BuildContext context) {
  return ThemeData(
    primaryColor: Colors.white,
    accentColor: kDarker.withOpacity(0.8),
    scaffoldBackgroundColor: kLight,
    backgroundColor: kLight,
    appBarTheme: appBarTheme,
    iconTheme: IconThemeData(color: kDarker.withOpacity(0.8)),
    textTheme: TextTheme(
      bodyText1: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: 0.4,
          fontWeight: FontWeight.w300),
      bodyText2: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: 0.4,
          fontWeight: FontWeight.w600),
      headline1: TextStyle(
        color: kDarker.withOpacity(0.7),
        fontFamily: kFontName,
        letterSpacing: 0.4,
      ),
      headline2: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: 0.4,
          fontWeight: FontWeight.w700),
      headline3: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: -0.05,
          fontWeight: FontWeight.w800),
      headline4: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: 0.4,
          fontWeight: FontWeight.w100),
      headline5: TextStyle(
          color: kDarker.withOpacity(0.7),
          fontFamily: kFontName,
          letterSpacing: 0.4,
          fontWeight: FontWeight.w200),
      caption: TextStyle(
        color: kDarker.withOpacity(0.7),
        fontFamily: kFontName,
        fontWeight: FontWeight.w400,
        letterSpacing: -0.05,
      ),
    ),
  );
}

ThemeData darkthemeData(BuildContext context) {
  return ThemeData.dark().copyWith(
    primaryColor: kDarker,
    accentColor: Colors.white,
    scaffoldBackgroundColor: kDarker.withOpacity(0.8),
    backgroundColor: kDarker.withOpacity(0.8),
    appBarTheme: appBarTheme,
    iconTheme: IconThemeData(color: Colors.white),
    textTheme: TextTheme(
      bodyText1: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.4,
      ),
      bodyText2: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w600,
        letterSpacing: 0.4,
      ),
      headline1: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        letterSpacing: 0.4,
      ),
      headline2: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w700,
        letterSpacing: 0.4,
      ),
      headline3: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w800,
        letterSpacing: -0.05,
      ),
      headline4: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w100,
        letterSpacing: 0.4,
      ),
      headline5: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w200,
        letterSpacing: 0.4,
      ),
      caption: TextStyle(
        color: kLight,
        fontFamily: kFontName,
        fontWeight: FontWeight.w400,
        letterSpacing: -0.05,
      ),
    ),
  );
}

AppBarTheme appBarTheme = AppBarTheme(color: Colors.transparent, elevation: 0);
