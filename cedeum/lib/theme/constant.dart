import 'package:flutter/material.dart';

const Color kWhite = Color(0xFFFEFEFE);

const Color kLight = Color(0xFFEEF1F3);
const Color kDarker = Color(0xFF17262A);
const String kFontName = 'Raleways';
