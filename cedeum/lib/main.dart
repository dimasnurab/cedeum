import 'package:bloc/bloc.dart';
import 'package:cedeum/pages/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'repositories/weather_repositories.dart';
import 'simplecubit_observer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = SimpleCubitObserver();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then(
    (_) => runApp(MyApp(repos: WeatherRepositories())),
  );
}
