import 'package:cedeum/repositories/weather_repositories.dart';
import 'package:cedeum/shared/routers.dart';
import 'package:cedeum/theme/bloc/theme_bloc.dart';
import 'package:cedeum/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class MyApp extends StatelessWidget {
  MyApp({this.repos}) : assert(repos != null);

  final WeatherRepositories repos;
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: repos,
      child: BlocProvider(
        create: (_) => ThemeBloc(repos: repos),
        child: MyAppView(),
      ),
    );
  }
}

class MyAppView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, state) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'CEDEUM',
          theme: themeData(context),
          darkTheme: darkthemeData(context),
          themeMode: state.status == ThemeStatus.light
              ? ThemeMode.light
              : ThemeMode.dark,
          initialRoute: Routers.initial,
          getPages: Routers().routes,
        );
      },
    );
  }
}
