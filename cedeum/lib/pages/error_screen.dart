import 'package:cedeum/models/errorhandle.dart';
import 'package:cedeum/widget/roundedbutton.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class ErrorScreen extends StatefulWidget {
  @override
  _ErrorScreenState createState() => _ErrorScreenState();
}

class _ErrorScreenState extends State<ErrorScreen> {
  ErrorHandle msg = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Ooops, ${msg.statusCode}',
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 26,
                    ),
              ),
              SizedBox(height: 30),
              Text(
                msg.message,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: 20,
                    ),
              ),
              ButtonRounded(
                  marginLeft: 42,
                  marginRight: 42,
                  marginTop: 30,
                  textButton: msg.statusCode.toString() == "404" ||
                          msg.statusCode.toString() == "400"
                      ? 'KEMBALI'
                      : 'REFRESH',
                  onTap: () => Get.back()),
            ],
          ),
        ),
      ),
    );
  }
}
