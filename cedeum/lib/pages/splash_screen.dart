import 'dart:async';

import 'package:cedeum/shared/routers.dart';
import 'package:cedeum/shared/simple_permission.dart';
import 'package:cedeum/widget/allerdialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:permission_handler/permission_handler.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  Timer _timer;

  @override
  void initState() {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 2000),
        reverseDuration: const Duration(milliseconds: 2000),
        vsync: this);

    _checkPerm();
    initTime();
    super.initState();
  }

  initTime() {
    _timer = Timer.periodic(new Duration(seconds: 10), (timer) {
      _checkPerm();
    });
  }

  _checkPerm() async {
    await Future.delayed(Duration(seconds: 4));
    PermissionServices services = PermissionServices();
    Permission location;

    location = Permission.location;

    PermissionStatus status1;

    status1 = await services.checkPermissionStatus(location);
    print(status1);
    if (status1.isGranted) {
      await _animationController.reverse();
      _navigateToHome();
    } else if (status1.isDenied ||
        status1.isUndetermined ||
        status1.isRestricted) {
      status1 = await services.requestPermission(location);
      if (status1.isGranted) {
        await _animationController.reverse();
        _navigateToHome();
      }
    } else if (status1.isPermanentlyDenied) {
      await _allert(
        title: 'Aplikasi ini memerlukan izin lokasi',
        confirm: () async {
          await openAppSettings().then((value) {
            Get.back();
          });
        },
      );
    }
  }

  _navigateToHome() => Get.offAllNamed(Routers.home);

  @override
  void dispose() {
    _animationController.dispose();
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.fastOutSlowIn));
    _animationController.forward();

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: LogoSplash(
            animation: _animation,
            animationController: _animationController,
          ),
        ),
      ),
    );
  }

  Future<bool> _allert({
    Function confirm,
    String title,
  }) {
    return showDialog(
          context: context,
          builder: (context) => allerDialogOnebutton(
            context: context,
            title: title,
            onTap: confirm,
          ),
        ) ??
        false;
  }
}

class LogoSplash extends StatelessWidget {
  final AnimationController animationController;
  final Animation animation;

  const LogoSplash({
    this.animation,
    this.animationController,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 30 * (1.0 - animation.value), 0.0),
            child: Container(
              child: Text(
                'CEDEUM',
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    .copyWith(fontSize: 32),
              ),
            ),
          ),
        );
      },
    );
  }
}
