import 'package:cedeum/pages/home/card_detail.dart';
import 'package:cedeum/pages/home/card_event.dart';
import 'package:cedeum/pages/home/card_sunrisesunset.dart';
import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class DetailsScreen extends StatefulWidget {
  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  dynamic item = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Stack(
            children: [
              Positioned(
                left: 10,
                top: 20,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          size: 24,
                          color: Theme.of(context).iconTheme.color,
                        ),
                        onPressed: () => Get.back()),
                    Text(
                      'KEMBALI',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontSize: 14),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 70, bottom: 20),
                        child: Text(
                            Utils().getCelcius(item['temp']['day'].toString()),
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                .copyWith(
                                    fontSize:
                                        getProportionateScreenHeight(96)))),
                    Container(
                        child: Text(
                            Utils().dateTimeFormatDays(item['dt']) +
                                ',' +
                                Utils().dateTimeFormatTgl(item['dt']),
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                .copyWith(
                                    fontSize: getProportionateScreenHeight(48),
                                    letterSpacing: 1.2))),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 42),
                      child: CardEvent(item: item),
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          flex: 1,
                          child: CardSunsetSunrise(
                            item: item,
                          ),
                        ),
                        Flexible(flex: 3, child: CardDetails(item: item))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
