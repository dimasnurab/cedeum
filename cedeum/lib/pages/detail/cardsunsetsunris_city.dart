import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:flutter/material.dart';

class CardSunsetSunriseCity extends StatefulWidget {
  final dynamic item;

  const CardSunsetSunriseCity({this.item});
  @override
  _CardSunsetSunriseCityState createState() => _CardSunsetSunriseCityState();
}

class _CardSunsetSunriseCityState extends State<CardSunsetSunriseCity>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 30 * (1.0 - animation.value), 0.0),
              child: Container(
                height: getProportionateScreenHeight(155),
                margin: EdgeInsets.fromLTRB(16, 0, 8, 20),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Theme.of(context).accentColor.withOpacity(0.2),
                      spreadRadius: 0.1,
                      blurRadius: 5,
                      offset: Offset(1, 2),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    textCustom(
                      title: 'Sunrise',
                      subtitle: Utils()
                          .timeformatterHHMM(widget.item['sys']['sunrise']),
                    ),
                    textCustom(
                      title: 'Sunset',
                      subtitle: Utils()
                          .timeformatterHHMM(widget.item['sys']['sunset']),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget textCustom({
    String title,
    String subtitle,
  }) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            overflow: TextOverflow.fade,
            maxLines: 1,
            style: Theme.of(context).textTheme.caption.copyWith(
                  fontSize: getProportionateScreenHeight(14),
                ),
          ),
          Text(
            subtitle,
            overflow: TextOverflow.fade,
            maxLines: 1,
            style: Theme.of(context).textTheme.headline3.copyWith(
                  fontSize: getProportionateScreenHeight(14),
                ),
          ),
        ],
      );
}
