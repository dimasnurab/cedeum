import 'package:cedeum/pages/detail/carddetails_city.dart';
import 'package:cedeum/pages/detail/cardsunsetsunris_city.dart';

import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class CityScreen extends StatefulWidget {
  @override
  _CityScreenState createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {
  dynamic item = Get.arguments;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Positioned(
              left: 10,
              top: 20,
              child: Row(
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 24,
                        color: Theme.of(context).iconTheme.color,
                      ),
                      onPressed: () => Get.back()),
                  Text(
                    'KEMBALI',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 14),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(
                          top: getProportionateScreenHeight(70),
                          bottom: getProportionateScreenHeight(30)),
                      child: Text(
                          Utils().getCelcius(item['main']['temp'].toString()),
                          style: Theme.of(context).textTheme.headline2.copyWith(
                                fontSize: 72,
                              ))),
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.place,
                        color: Theme.of(context).iconTheme.color,
                        size: getProportionateScreenHeight(32),
                      ),
                      SizedBox(width: 4),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(item['name'],
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  .copyWith(
                                    fontSize: 16,
                                  )),
                          Text(item['weather'][0]['description'],
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(fontSize: 12)),
                        ],
                      ),
                    ],
                  )),
                  Container(
                      margin: EdgeInsets.only(
                          top: getProportionateScreenHeight(20)),
                      child: Text(
                          Utils().dateTimeFormatDays(item['dt']) +
                              ',' +
                              Utils().dateTimeFormatTgl(item['dt']),
                          style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: getProportionateScreenHeight(52),
                              letterSpacing: 1.2))),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 1,
                        child: CardSunsetSunriseCity(
                          item: item,
                        ),
                      ),
                      Flexible(flex: 3, child: CardDetailCity(item: item))
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
