import 'package:cedeum/models/feelslike.dart';
import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:flutter/material.dart';

class CardEvent extends StatefulWidget {
  final dynamic item;

  const CardEvent({this.item});
  @override
  _CardEventState createState() => _CardEventState();
}

class _CardEventState extends State<CardEvent> with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<FeelsLike> item = widget.item['feels_like'].entries
        .map<FeelsLike>((e) => FeelsLike(key: e.key, value: e.value))
        .toList();
    SizeConfig().init(context);
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 30 * (1.0 - animation.value), 0.0),
              child: Container(
                padding: EdgeInsets.all(14),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: item
                      .map((e) => textCustom(
                            title: Utils().getTimeFellsLike(e.key),
                            subtitle: Utils().getCelcius(e.value.toString()),
                          ))
                      .toList(),
                ),
              ),
            ),
          );
        });
  }

  Widget textCustom({
    String title,
    String subtitle,
  }) =>
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.caption.copyWith(
                fontSize: getProportionateScreenHeight(15), letterSpacing: 1.2),
          ),
          Text(
            subtitle,
            style: Theme.of(context)
                .textTheme
                .headline3
                .copyWith(fontSize: getProportionateScreenHeight(15)),
          ),
        ],
      );
}
