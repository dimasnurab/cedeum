import 'package:cedeum/shared/size_config.dart';
import 'package:flutter/material.dart';

class CardDetails extends StatefulWidget {
  final dynamic item;

  const CardDetails({this.item});
  @override
  _CardDetailsState createState() => _CardDetailsState();
}

class _CardDetailsState extends State<CardDetails>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 30 * (1.0 - animation.value), 0.0),
              child: Container(
                height: getProportionateScreenHeight(155),
                margin: EdgeInsets.only(right: 16),
                padding: EdgeInsets.all(14),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Theme.of(context).accentColor.withOpacity(0.2),
                      spreadRadius: 0.1,
                      blurRadius: 5,
                      offset: Offset(1, 2),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    textCustom(
                      title: 'Humidity',
                      subtitle: widget.item['humidity'].toString() + '%',
                    ),
                    textCustom(
                      title: 'Pressure',
                      subtitle: widget.item['pressure'].toString() + 'mb',
                    ),
                    textCustom(
                      title: 'wind_deg',
                      subtitle: widget.item['wind_deg'].toString() + 'km/h',
                    ),
                    textCustom(
                        title: 'windspeed',
                        subtitle:
                            widget.item['wind_speed'].toString() + 'km/h'),
                    textCustom(
                      title: 'chance to rain',
                      subtitle: widget.item['rain'].toString() + '%',
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget textCustom({
    String title,
    String subtitle,
  }) =>
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.caption.copyWith(
                  fontSize: getProportionateScreenHeight(14),
                  letterSpacing: 0.8,
                ),
          ),
          Text(
            subtitle,
            style: Theme.of(context).textTheme.headline3.copyWith(
                  fontSize: getProportionateScreenHeight(14),
                ),
          ),
        ],
      );
}
