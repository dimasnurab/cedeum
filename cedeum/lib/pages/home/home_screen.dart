import 'dart:io';
import 'dart:ui';

import 'package:bezier_chart/bezier_chart.dart';
import 'package:cedeum/pages/home/card_detail.dart';
import 'package:cedeum/pages/home/card_event.dart';
import 'package:cedeum/pages/home/card_sunrisesunset.dart';
import 'package:cedeum/pages/home/card_list.dart';
import 'package:cedeum/pages/home/cubit/home_cubit.dart';
import 'package:cedeum/repositories/weather_repositories.dart';
import 'package:cedeum/shared/routers.dart';
import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:cedeum/theme/bloc/theme_bloc.dart';
import 'package:cedeum/theme/constant.dart';
import 'package:cedeum/widget/allerdialog_custom.dart';
import 'package:cedeum/widget/dots_pageview.dart';
import 'package:cedeum/widget/drawerallwidget.dart';
import 'package:cedeum/widget/roundedbutton.dart';
import 'package:cedeum/widget/slidingupcustom.dart';
import 'package:cedeum/widget/stackwith_progress.dart';
import 'package:cedeum/widget/textformfield_custom.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  HomeCubit _cubit;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _cubit = context.read<HomeCubit>();
    _cubit.fetchWeatherCurrentPosisition();

    super.initState();
  }

  _detectBottom() => !(MediaQuery.of(context).viewInsets.bottom == 00);

  _onDetailsCity(dynamic val) => Get.toNamed(Routers.city, arguments: val);

  _tapButtonOkdialog(HomeState state) {
    if (state.errorMsg.statusCode.toString() != '500') {
      Get.back();
    } else {
      Get.back();
      _cubit.fetchWeatherCurrentPosisition();
    }
  }

  _openDrawer() {
    _scaffoldKey.currentState.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    DateTime _time = DateTime.now();
    SizeConfig().init(context);

    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: buildDrawer(),
        drawerEnableOpenDragGesture: false,
        body: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {
            if (state.status == HomeStateStatus.success) {
              _onDetailsCity(state.dataCity);
            }
            if (state.status == HomeStateStatus.fail) {
              _allert(
                titleButton: state.errorMsg.statusCode.toString() == '500'
                    ? 'MUAT ULANG'
                    : 'TUTUP',
                msg: state.errorMsg.message,
                statusCode: state.errorMsg.statusCode,
                onTap: () {
                  _tapButtonOkdialog(state);
                },
              );
            }
            if (state.themeState) {
              BlocProvider.of<ThemeBloc>(context)
                  .add(ChangeTheme(ThemeStatus.dark));
            } else {
              BlocProvider.of<ThemeBloc>(context)
                  .add(ChangeTheme(ThemeStatus.light));
            }
          },
          builder: (context, state) {
            PageController _pageController =
                PageController(initialPage: state.indexPageView);
            return StackWithProgress(
              isLoading: state.status == HomeStateStatus.loading,
              children: [
                buildIconDrawer(),
                Container(
                  margin:
                      EdgeInsets.only(top: getProportionateScreenHeight(60)),
                  child: SingleChildScrollView(
                    child: state.current == null
                        ? Container()
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: getProportionateScreenHeight(10)),
                              buildTimeOfDay(_time, context),
                              buildTemperature(state, _time, context),
                              buildLocationAndDesc(context, state),
                              Container(
                                width: double.infinity,
                                height: getProportionateScreenHeight(240),
                                child: PageView(
                                  children: [
                                    buildCardSunsetAndDetails(state),
                                    buildChart(state, context),
                                  ],
                                  physics: BouncingScrollPhysics(),
                                  controller: _pageController,
                                  onPageChanged: (val) {
                                    _cubit.changeIndexPageView(val);
                                  },
                                ),
                              ),
                              Container(
                                child: DotsPageView(
                                  currentIndex: state.indexPageView,
                                  dotsItems: 2,
                                ),
                              ),
                              CardDaysListview(
                                cubit: _cubit,
                                currentIndex: state.indexDays,
                                item: state.daily,
                              ),
                            ],
                          ),
                  ),
                ),
                state.slidingUp ? buildBackdropBlur(context) : Container(),
                SlidingUpCustom(
                  isOpenedPanel: state.slidingUp,
                  maxHeight: _detectBottom() ? 280 : 240,
                  minHeight: getProportionateScreenHeight(120),
                  children: [
                    Container(
                      child: state.slidingUp
                          ? Container(
                              height: 10,
                              width: 50,
                              margin: EdgeInsets.only(bottom: 10, top: 10),
                              decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.circular(24),
                              ),
                            )
                          : Icon(
                              Icons.arrow_drop_up,
                              size: 48,
                              color: Theme.of(context).iconTheme.color,
                            ),
                    ),
                    Container(
                      child: Text(
                        'Pencarian Kota',
                        style: Theme.of(context).textTheme.headline3.copyWith(
                              fontSize: 16,
                            ),
                      ),
                    ),
                    CustomTextFormfield(
                      marginTop: 20,
                      colorBorder: Theme.of(context).accentColor,
                      backgroundColorField: Theme.of(context).primaryColor,
                      widthBorder: 2,
                      paddingLeft: 14,
                      hintText: 'Cari kota...',
                      hintStyle: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(fontSize: 12),
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(fontSize: 12),
                      onChanged: (val) {
                        _cubit.changeValueCity(val);
                      },
                    ),
                    ButtonRounded(
                      marginTop: 30,
                      textButton: 'CARI',
                      onTap: () {
                        _cubit.fetchByCity();
                      },
                    ),
                  ],
                  onPanelClosed: () {
                    _cubit.slidingClosed();
                  },
                  onPanelOpened: () {
                    _cubit.slidingOpened();
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Drawer buildDrawer() {
    return Drawer(
      elevation: 0,
      child: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) {
          return Container(
            color: Theme.of(context).primaryColor,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  drawerheadercustom(context),
                  builditemdrawer(
                    context,
                    title: 'Ganti Tema',
                    titleStyle: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontSize: 12),
                    child: Platform.isAndroid
                        ? Switch(
                            inactiveThumbColor: kLight,
                            inactiveTrackColor: kDarker.withOpacity(0.8),
                            activeColor: kLight,
                            activeTrackColor: kLight,
                            value: state.themeState,
                            onChanged: (val) {
                              _cubit.changeTheme(val);
                            })
                        : CupertinoSwitch(
                            trackColor: kDarker.withOpacity(0.8),
                            activeColor: kLight,
                            value: state.themeState,
                            onChanged: (val) {
                              _cubit.changeTheme(val);
                            }),
                  ),
                  SizedBox(height: 6),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(Routers.clock);
                    },
                    child: builditemdrawer(
                      context,
                      title: 'Aplkasi Jam',
                      titleStyle: Theme.of(context)
                          .textTheme
                          .bodyText2
                          .copyWith(fontSize: 12),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        size: 14,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
                  SizedBox(height: getProportionateScreenHeight(380)),
                  buildDivider(context),
                  builditemdrawer(context, title: 'Version 1.0.1'),
                  SizedBox(height: 10),
                  builditemdrawer(context,
                      title:
                          'Designed & Developed by\nDimas Nur Abdillah Hidayat© 2020'),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Container buildDivider(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 14, vertical: 8),
      child: Divider(
        height: 1,
        color: Theme.of(context).accentColor,
      ),
    );
  }

  Positioned buildIconDrawer() {
    return Positioned(
      left: 10,
      top: 10,
      child: IconButton(
        icon: Icon(Icons.menu),
        onPressed: _openDrawer,
        iconSize: 24,
        color: Theme.of(context).accentColor,
      ),
    );
  }

  BackdropFilter buildBackdropBlur(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 6,
        sigmaY: 6,
      ),
      child: Container(
        color: Theme.of(context).primaryColor.withOpacity(0.6),
      ),
    );
  }

  Container buildChart(HomeState state, BuildContext context) => Container(
        margin: EdgeInsets.only(right: 20, left: 20),
        width: double.infinity,
        height: getProportionateScreenHeight(240),
        color: Colors.transparent,
        child: BezierChart(
          bezierChartAggregation: BezierChartAggregation.FIRST,
          bezierChartScale: BezierChartScale.WEEKLY,
          bubbleLabelDateTimeBuilder:
              (DateTime value, BezierChartScale scaleType) {
            return Utils().dateTimeFormatDays2(value) + '\n';
          },
          footerDateTimeBuilder: (DateTime value, BezierChartScale scaleType) {
            return Utils().dateTimeFormatDays2(value);
          },
          fromDate:
              DateTime.fromMillisecondsSinceEpoch(state.daily[0]['dt'] * 1000),
          toDate:
              DateTime.fromMillisecondsSinceEpoch(state.daily[5]['dt'] * 1000),
          series: [
            BezierLine(
              label: "Humidity",
              lineColor: Colors.blue,
              lineStrokeWidth: 2.0,
              data: [
                for (var item in state.daily)
                  DataPoint<DateTime>(
                      value: item['humidity'] != null
                          ? double.parse(item['humidity'].round().toString())
                          : 0,
                      xAxis: DateTime.fromMillisecondsSinceEpoch(
                          item['dt'] * 1000)),
              ],
            ),
            BezierLine(
              label: "Temperature",
              lineColor: Colors.teal,
              lineStrokeWidth: 2.0,
              data: [
                for (var item in state.daily)
                  DataPoint<DateTime>(
                      value: item['temp']['day'] != null
                          ? double.parse(item['temp']['day']
                              .round()
                              .toString()
                              .substring(0, 2))
                          : 0,
                      xAxis: DateTime.fromMillisecondsSinceEpoch(
                          item['dt'] * 1000)),
              ],
            ),
          ],
          config: BezierChartConfig(
            displayDataPointWhenNoValue: false,
            verticalIndicatorFixedPosition: true,
            physics: NeverScrollableScrollPhysics(),
            showVerticalIndicator: false,
            bubbleIndicatorColor: Theme.of(context).accentColor,
            bubbleIndicatorLabelStyle: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(
                    fontSize: getProportionateScreenWidth(14),
                    color: Theme.of(context).primaryColor),
            bubbleIndicatorTitleStyle: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(
                    fontSize: getProportionateScreenWidth(14),
                    color: Theme.of(context).primaryColor),
            bubbleIndicatorValueStyle: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(
                    fontSize: getProportionateScreenWidth(14),
                    color: Theme.of(context).primaryColor),
            xAxisTextStyle: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(fontSize: getProportionateScreenWidth(14)),
          ),
        ),
      );

  Column buildCardSunsetAndDetails(HomeState state) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(42, 0, 42, 0),
          child: CardEvent(item: state.current),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              flex: 1,
              child: CardSunsetSunrise(
                item: state.current,
              ),
            ),
            Flexible(flex: 3, child: CardDetails(item: state.current))
          ],
        ),
      ],
    );
  }

  Container buildLocationAndDesc(BuildContext context, HomeState state) =>
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              Icons.place,
              color: Theme.of(context).iconTheme.color,
              size: getProportionateScreenHeight(32),
            ),
            SizedBox(width: 4),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(state.timeZone,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontSize: 16,
                        )),
                Text(state.current['weather'][0]['description'],
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12)),
              ],
            ),
          ],
        ),
      );

  Widget buildTemperature(
          HomeState state, DateTime _time, BuildContext context) =>
      Container(
        margin: EdgeInsets.only(bottom: getProportionateScreenHeight(30)),
        child: Text(
          Utils().getCelcius(state.current['temp']
                  [Utils().getCelciusbyJson(_time.hour, _time.minute)]
              .toString()),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline2.copyWith(
                fontSize: getProportionateScreenHeight(102),
              ),
        ),
      );

  Widget buildTimeOfDay(DateTime _time, BuildContext context) => Container(
        child: Text(
          Utils().getTimeofDay(_time.hour, _time.minute),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline5.copyWith(
              fontSize: getProportionateScreenHeight(32), letterSpacing: 1.2),
        ),
      );

  Future<bool> _allert(
      {Function onTap, dynamic msg, dynamic statusCode, String titleButton}) {
    return showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => allertFailedStatus(
            context: context,
            onTap: onTap,
            msg: msg,
            statusCode: statusCode,
            titleButton: titleButton,
            height: getProportionateScreenHeight(375),
            width: double.infinity,
          ),
        ) ??
        false;
  }
}
