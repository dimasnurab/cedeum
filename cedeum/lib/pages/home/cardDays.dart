import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:flutter/material.dart';

class CardDays extends StatelessWidget {
  final dynamic item;
  final AnimationController animationController;
  final Animation<dynamic> animation;
  final Function(dynamic, int) onTap;
  final bool selected;
  final int index;

  const CardDays({
    this.index,
    this.animation,
    this.selected,
    this.animationController,
    this.item,
    this.onTap,
  });

  _onTap() {
    onTap(item, index);
  }

  @override
  Widget build(BuildContext context) {
    var date = item['dt'];
    SizeConfig().init(context);

    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                100 * (1.0 - animation.value), 0.0, 0.0),
            child: GestureDetector(
              onTap: _onTap,
              child: Container(
                margin: EdgeInsets.only(right: 8, bottom: 4),
                padding: EdgeInsets.all(getProportionateScreenHeight(4)),
                width: getProportionateScreenWidth(70),
                decoration: BoxDecoration(
                  color: selected
                      ? Theme.of(context).accentColor.withOpacity(0.8)
                      : Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Theme.of(context).accentColor.withOpacity(0.1),
                      spreadRadius: 0.1,
                      blurRadius: 5,
                      offset: Offset(1, 2),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      Utils().dateTimeFormatDays(date),
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.caption.copyWith(
                            fontSize: getProportionateScreenHeight(16),
                            color: selected
                                ? Theme.of(context).primaryColor
                                : Theme.of(context).accentColor,
                          ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(4),
                    ),
                    Text(
                      Utils().dateTimeFormatTgl(date),
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.caption.copyWith(
                            fontSize: getProportionateScreenHeight(14),
                            color: selected
                                ? Theme.of(context).primaryColor
                                : Theme.of(context).accentColor,
                          ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
