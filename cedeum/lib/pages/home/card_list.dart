import 'package:cedeum/pages/home/cardDays.dart';
import 'package:cedeum/pages/home/cubit/home_cubit.dart';
import 'package:cedeum/shared/routers.dart';
import 'package:cedeum/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class CardDaysListview extends StatefulWidget {
  final dynamic item;
  final int currentIndex;
  final HomeCubit cubit;

  const CardDaysListview({
    this.item,
    this.cubit,
    this.currentIndex,
  });

  @override
  _CardDaysListviewState createState() => _CardDaysListviewState();
}

class _CardDaysListviewState extends State<CardDaysListview>
    with TickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  _navigateToDetails(dynamic val) =>
      Get.toNamed(Routers.detail, arguments: val);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Container(
      width: double.infinity,
      height: getProportionateScreenHeight(100),
      padding: const EdgeInsets.only(top: 0, right: 16, left: 16),
      child: ListView.builder(
        itemCount: widget.item.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          final Animation<double> animation =
              Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                  parent: animationController,
                  curve: Interval((1 / widget.item.length) * index, 1.0,
                      curve: Curves.fastOutSlowIn)));
          animationController.forward();
          return CardDays(
            animation: animation,
            animationController: animationController,
            item: widget.item[index],
            index: index,
            selected: index == widget.currentIndex,
            onTap: (item, index) {
              _navigateToDetails(item);
              widget.cubit.changeIndexdays(index);
            },
          );
        },
      ),
    );
  }
}
