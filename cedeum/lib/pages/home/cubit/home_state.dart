part of 'home_cubit.dart';

enum HomeStateStatus { fail, loading, success, initial }

class HomeState extends Equatable {
  HomeState({
    this.current,
    this.daily,
    this.status = HomeStateStatus.loading,
    this.errorMsg,
    this.slidingUp = false,
    this.timeZone,
    this.cityName,
    this.dataCity,
    this.themeState = false,
    this.indexPageView = 0,
    this.indexDays = 0,
  });

  final int indexPageView;
  final dynamic daily;
  final ErrorHandle errorMsg;
  final dynamic current;
  final HomeStateStatus status;
  final bool slidingUp;
  final String timeZone;
  final String cityName;
  final dynamic dataCity;
  final bool themeState;
  final int indexDays;

  HomeState update({
    bool themeState,
    dynamic daily,
    dynamic current,
    ErrorHandle errorMsg,
    HomeStateStatus status,
    bool slidingUp,
    String timeZone,
    String cityName,
    dynamic dataCity,
    int indexPageView,
    int indexDays,
  }) =>
      copyWith(
        daily: daily,
        current: current,
        status: status,
        errorMsg: errorMsg,
        slidingUp: slidingUp,
        timeZone: timeZone,
        cityName: cityName,
        dataCity: dataCity,
        themeState: themeState,
        indexPageView: indexPageView,
        indexDays: indexDays,
      );

  HomeState copyWith({
    String cityName,
    bool themeState,
    dynamic dataCity,
    dynamic daily,
    ErrorHandle errorMsg,
    dynamic current,
    HomeStateStatus status,
    bool slidingUp,
    String timeZone,
    int indexPageView,
    int indexDays,
  }) =>
      HomeState(
        daily: daily ?? this.daily,
        current: current ?? this.current,
        status: status ?? this.status,
        errorMsg: errorMsg ?? this.errorMsg,
        slidingUp: slidingUp ?? this.slidingUp,
        timeZone: timeZone ?? this.timeZone,
        cityName: cityName ?? this.cityName,
        dataCity: dataCity ?? this.dataCity,
        themeState: themeState ?? this.themeState,
        indexPageView: indexPageView ?? this.indexPageView,
        indexDays: indexDays ?? this.indexDays,
      );

  @override
  List<Object> get props => [
        daily,
        status,
        current,
        errorMsg,
        slidingUp,
        timeZone,
        dataCity,
        cityName,
        themeState,
        indexPageView,
        indexDays,
      ];
}
