import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cedeum/models/errorhandle.dart';
import 'package:cedeum/repositories/weather_repositories.dart';
import 'package:cedeum/services/networkexception.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeState());

  WeatherRepositories _repos = WeatherRepositories();

  changeTheme(bool val) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool('theme', val);
    emit(state.update(
      themeState: val,
      status: HomeStateStatus.initial,
    ));
  }

  changeIndexPageView(int val) => emit(state.update(
        status: HomeStateStatus.initial,
        indexPageView: val,
      ));

  changeIndexdays(int val) => emit(state.update(
        status: HomeStateStatus.initial,
        indexDays: val,
      ));

  slidingOpened() => emit(state.update(
        slidingUp: true,
        status: HomeStateStatus.initial,
      ));

  slidingClosed() => emit(state.update(
        slidingUp: false,
        status: HomeStateStatus.initial,
      ));

  changeValueCity(String name) => emit(state.update(
        cityName: name,
        status: HomeStateStatus.initial,
      ));

  fetchWeatherCurrentPosisition() async {
    emit(state.update(status: HomeStateStatus.loading));
    try {
      await Future.delayed(const Duration(milliseconds: 750));
      final _response = await _repos.fetchCurrentPosition();
      var stateTheme = await _repos.getTheme();
      emit(state.update(
        themeState: stateTheme,
        daily: _response.data['daily'],
        current: _response.data['daily'][0],
        timeZone: _response.data['timezone'],
        status: HomeStateStatus.initial,
      ));
    } on NetworkException catch (e) {
      emit(state.update(
        status: HomeStateStatus.fail,
        errorMsg: ErrorHandle(
          statusCode: e.httpStatus,
          message: e.responseMessage,
        ),
      ));
    } catch (e) {
      emit(state.update(
        status: HomeStateStatus.fail,
        errorMsg: ErrorHandle(
          statusCode: 000,
          message: 'Terjadi Kesalahan',
        ),
      ));
    }
  }

  fetchByCity() async {
    emit(state.update(
        status: HomeStateStatus.loading,
        slidingUp: false,
        indexDays: 0,
        indexPageView: 0));
    try {
      final _response = await _repos.fetchByCity(state.cityName);

      emit(state.update(
        dataCity: _response.data,
        status: HomeStateStatus.success,
        cityName: '',
      ));
    } on NetworkException catch (e) {
      emit(state.update(
        status: HomeStateStatus.fail,
        errorMsg: ErrorHandle(
          statusCode: e.httpStatus,
          message: e.responseMessage,
        ),
      ));
    } catch (e) {
      emit(state.update(
          status: HomeStateStatus.fail,
          errorMsg: ErrorHandle(
            statusCode: 000,
            message: 'Terjadi kesalahan',
          )));
    }
  }
}
