import 'package:cedeum/shared/size_config.dart';
import 'package:cedeum/shared/utils.dart';
import 'package:cedeum/theme/constant.dart';
import 'package:flutter/material.dart';

class DigitalClock extends StatelessWidget {
  const DigitalClock({
    Key key,
    @required DateTime dateTime,
  })  : _dateTime = dateTime,
        super(key: key);

  final DateTime _dateTime;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AspectRatio(
      aspectRatio: 3,
      child: Container(
        padding: EdgeInsets.all(14),
        margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenHeight(20),
            vertical: getProportionateScreenHeight(10)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: kWhite,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 0),
              color: kDarker.withOpacity(0.14),
              blurRadius: 64,
            )
          ],
        ),
        child: Container(
          padding: EdgeInsets.only(bottom: 4, left: 4),
          decoration: BoxDecoration(
              color: kDarker.withOpacity(0.7),
              borderRadius: BorderRadius.circular(12)),
          child: Row(
            children: [
              Text(
                Utils().clokcFormatHHMM(_dateTime),
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 54,
                      letterSpacing: 10,
                      color: kLight.withOpacity(0.7),
                    ),
              ),
              SizedBox(width: 8),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 6),
                  Text(
                    Utils().getPMAM(_dateTime.hour, _dateTime.minute),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 18,
                          letterSpacing: 10,
                          color: kLight.withOpacity(0.7),
                        ),
                  ),
                  Text(
                    Utils().dateTimeFormatDays2(_dateTime),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 18,
                          color: kLight.withOpacity(0.7),
                        ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
