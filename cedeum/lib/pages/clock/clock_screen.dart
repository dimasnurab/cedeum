import 'dart:async';

import 'package:cedeum/theme/constant.dart';
import 'package:cedeum/widget/stackwith_progress.dart';
import 'package:flutter/material.dart';

import 'digital_clock.dart';

class ClockScreen extends StatefulWidget {
  @override
  _ClockScreenState createState() => _ClockScreenState();
}

class _ClockScreenState extends State<ClockScreen> {
  DateTime _dateTime = DateTime.now();
  Timer _timer;
  @override
  void initState() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _dateTime = DateTime.now();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: kLight,
        body: StackWithProgress(
          children: [DigitalClock(dateTime: _dateTime)],
        ),
      ),
    );
  }
}
