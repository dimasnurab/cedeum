class ErrorHandle {
  dynamic statusCode;
  dynamic message;

  ErrorHandle({this.message, this.statusCode});

  @override
  String toString() => 'statusCode : $statusCode message $message';
}
