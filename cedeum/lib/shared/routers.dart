import 'package:cedeum/pages/clock/clock_screen.dart';
import 'package:cedeum/pages/detail/cityScreen.dart';
import 'package:cedeum/pages/detail/detailsscreen.dart';
import 'package:cedeum/pages/home/cubit/home_cubit.dart';
import 'package:cedeum/pages/home/home_screen.dart';
import 'package:cedeum/pages/splash_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';

class Routers {
  static const initial = '/';
  static const home = '/home';
  static const detail = '/detail';
  static const city = '/city';
  static const error = '/error';
  static const clock = '/clock';

  List<GetPage> routes = [
    GetPage(
      name: initial,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: home,
      page: () => BlocProvider<HomeCubit>(
        create: (_) => HomeCubit(),
        child: HomeScreen(),
      ),
    ),
    GetPage(
      name: detail,
      page: () => DetailsScreen(),
    ),
    GetPage(
      name: city,
      page: () => CityScreen(),
    ),
    GetPage(
      name: clock,
      page: () => ClockScreen(),
    ),
  ];
}
