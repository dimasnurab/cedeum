import 'package:intl/intl.dart';

class Utils {
  dateTimeFormatDays(int value) {
    DateFormat formatter = DateFormat('EEEE');
    final date = DateTime.fromMillisecondsSinceEpoch(value * 1000);
    final result = formatter.format(date);
    return _daysToindo(result);
  }

  dateTimeFormatDays2(DateTime value) {
    DateFormat formatter = DateFormat('EEEE');
    final result = formatter.format(value);
    return _daysToindo(result);
  }

  dateTimeFormatTgl(int value) {
    DateFormat formatter = DateFormat('dd');
    final date = DateTime.fromMillisecondsSinceEpoch(value * 1000);
    final result = formatter.format(date);
    return result;
  }

  timeformatterHHMM(int value) {
    DateFormat formatter = DateFormat('HH : mm');
    final date = DateTime.fromMillisecondsSinceEpoch(value * 1000);
    final result = formatter.format(date);
    return result;
  }

  clokcFormatHHMM(DateTime time) {
    DateFormat formatter = DateFormat('HH:mm');
    final result = formatter.format(time);
    return result;
  }

  getCelcius(String value) {
    String round = double.parse(value).round().toString();
    var result = round.substring(0, 2);
    return result + '°';
  }

  getPMAM(hour, minute) {
    if (hour >= 0 && hour <= 12 && minute >= 0 && minute <= 59) {
      return "AM";
    } else if (hour >= 12 && hour <= 24 && minute >= 0 && minute <= 59) {
      return "PM";
    }
  }

  getTimeofDay(hour, minute) {
    if (hour >= 0 && hour <= 10 && minute >= 0 && minute <= 59) {
      return "Selamat Pagi";
    } else if (hour >= 11 && hour <= 14 && minute >= 0 && minute <= 59) {
      return "Selamat Siang";
    } else if (hour >= 15 && hour <= 17 && minute >= 0 && minute <= 59) {
      return "Selamat Sore";
    } else if (hour >= 18 && hour <= 23 && minute >= 0 && minute <= 59) {
      return "Selamat Malam";
    }
  }

  getCelciusbyJson(hour, minute) {
    if (hour >= 0 && hour <= 10 && minute >= 0 && minute <= 59) {
      return "morn";
    } else if (hour >= 11 && hour <= 14 && minute >= 0 && minute <= 59) {
      return "day";
    } else if (hour >= 15 && hour <= 17 && minute >= 0 && minute <= 59) {
      return "eve";
    } else if (hour >= 18 && hour <= 23 && minute >= 0 && minute <= 59) {
      return "night";
    }
  }

  getTimeFellsLike(String item) {
    switch (item.toLowerCase()) {
      case 'day':
        return 'Siang';
        break;
      case 'night':
        return 'Malam';
        break;
      case 'eve':
        return 'Petang';
        break;
      case 'morn':
        return 'Pagi';
        break;
      default:
        return 'undefined';
    }
  }

  _daysToindo(String result) {
    switch (result.toLowerCase()) {
      case 'sunday':
        return 'Minggu';
        break;
      case 'monday':
        return 'Senin';
        break;
      case 'tuesday':
        return 'Selasa';
        break;
      case 'wednesday':
        return 'Rabu';
        break;
      case 'thursday':
        return 'Kamis';
        break;
      case 'friday':
        return 'Jumat';
        break;
      case 'saturday':
        return 'Sabtu';
        break;
      default:
        return 'undefined';
    }
  }
}
